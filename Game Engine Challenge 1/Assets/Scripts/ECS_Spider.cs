﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

// Author: Saran Krishnaraja 100621699
// Code based on Mathooshan's work on Spider Script

public class ECS_Spider : MonoBehaviour
{
    const string DLL_NAME = "EntityComponentSystem";

    [DllImport(DLL_NAME)]
    private static extern void LoadDLL();

    [DllImport(DLL_NAME)]
    private static extern int Create();

    [DllImport(DLL_NAME)]
    private static extern void SetPos(int id, float x, float y, float z);

    [DllImport(DLL_NAME)]
    private static extern float GetX(int id);

    [DllImport(DLL_NAME)]
    private static extern float GetY(int id);

    [DllImport(DLL_NAME)]
    private static extern float GetZ(int id);

    int id;

    public float movespeed;

    // Use this for initialization
    float maxValue; // or whatever you want the max value to be
    float minValue; // or whatever you want the min value to be
    float currentValue; // or wherever you want to start
    float direction;

    Vector3 tempPos;

    private void Start()
    {
        LoadDLL();
        id = Create();

        maxValue = 5.0f; // or whatever you want the max value to be
        minValue = -5.0f; // or whatever you want the min value to be
        direction = 1.0f;

        SetPos(id, transform.position.x, transform.position.y, transform.position.z);
    }

    private void Update()
    {
        if (!Input.GetMouseButton(0))
        {
            currentValue += Time.deltaTime * direction * movespeed; // or however you are incrementing the position

            if (currentValue >= maxValue)
            {
                direction *= -1;
                currentValue = maxValue;
            }
            else if (currentValue <= minValue)
            {
                direction *= -1;
                currentValue = minValue;
            }

            tempPos = new Vector3(currentValue, transform.position.y, 0);   // Save the Position

            SetPos(id, tempPos.x, tempPos.y, tempPos.z);                    // Set the Position

            transform.position = new Vector3(GetX(id), GetY(id), GetZ(id)); // Get the X,Y,Z
        }
    }
}
