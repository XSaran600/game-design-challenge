﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Author: Saran Krishnaraja 100621699

public class Ship : MonoBehaviour {

    public GameObject bulletPrefab;

    public AudioSource source;
    public AudioClip shootSound;


	// Use this for initialization
	void Start () {
        source.clip = shootSound;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        InputHandler();
	}

    void InputHandler()
    {
        if(Input.GetKeyDown(KeyCode.A))
        {
            transform.Translate(-0.5f, 0.0f, 0.0f);
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            transform.Translate(0.5f, 0.0f, 0.0f);
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Fire();
            source.Play();
        }
    }

    void Fire()
    {
        GameObject bullet = Instantiate(bulletPrefab, new Vector3(transform.position.x, transform.position.y + 1.0f, transform.position.z), transform.rotation);

        bullet.GetComponent<Rigidbody>().velocity = bullet.transform.up * 6;

        Destroy(bullet, 5.0f);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            Destroy(gameObject);
        }
    }
}
