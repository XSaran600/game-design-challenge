﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Author: Saran Krishnaraja 100621699

public class Centipedebehvior : MonoBehaviour {

    public float moveSpeed;

    private void Update()
    {
        if (!Input.GetMouseButton(0))
        {
            transform.position = new Vector3(transform.position.x, transform.position.y - 0.1f * moveSpeed, 0);
        }
    }
}
