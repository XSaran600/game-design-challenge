﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Author: Mathooshan Thevakumaran 100553777
// Modifed by: Saran Krishnaraja 100621699

public class Spider : MonoBehaviour
{

    public float movespeed;

    // Use this for initialization
    float maxValue; // or whatever you want the max value to be
    float minValue; // or whatever you want the min value to be
    float currentValue; // or wherever you want to start
    float direction;

    void Start()
    {
        maxValue = 5.0f; // or whatever you want the max value to be
        minValue = -5.0f; // or whatever you want the min value to be
        direction = 1.0f;
    }

    void Update()
    {
        if (!Input.GetMouseButton(0))
        {
            currentValue += Time.deltaTime * direction * movespeed; // or however you are incrementing the position

            if (currentValue >= maxValue)
            {
                direction *= -1;
                currentValue = maxValue;
            }
            else if (currentValue <= minValue)
            {
                direction *= -1;
                currentValue = minValue;
            }
            transform.position = new Vector3(currentValue, transform.position.y, 0);
        }
    }
}
