﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.Runtime.InteropServices;

// Author: Saran Krishnaraja 100621699

public class CommandSpider : MonoBehaviour
{

    const string DLL_NAME = "ExampleDLL";

    [DllImport(DLL_NAME)]
    private static extern void LoadDLL();

    [DllImport(DLL_NAME)]
    private static extern void Execute(ref float x, ref float y);

    [DllImport(DLL_NAME)]
    private static extern void Redo(ref float x, ref float y);

    public GameObject tempPrefab;


    private GameObject temp;
    private Vector3 tempPos;

    private bool undo = false;

    private float distance;
    private bool spawned = false;

    private void Start()
    {
        LoadDLL();
        spawned = false;
        undo = false;
    }

    void Update()
    {
        InputHandler();
    }

    void InputHandler()
    {

        // Left Click is pressed down
        if (Input.GetMouseButton(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            distance = Vector3.Distance(transform.position, Camera.main.transform.position);
            Vector3 rayPoint = ray.GetPoint(distance);

            // If the mouse is clicking on a object
            if (Physics.Raycast(ray, out hit, 1000.0f))
            {
                // If you hit tempSpawn and check if you spawned it (makes sure it only spawns one at a time)
                if (hit.transform.gameObject.ToString() == "SpiderSpawn (UnityEngine.GameObject)" && spawned == false)
                {
                    temp = Instantiate(tempPrefab);     // Create the prebaf
                    spawned = true;
                }

                // Undo the last spawn if you spawned something
                if (hit.transform.gameObject.ToString() == "SpiderSpawnUndo (UnityEngine.GameObject)" && temp != null)
                {
                    Destroy(temp);
                    undo = true;
                }

                // Redo the last spawn if you did an undo
                if (hit.transform.gameObject.ToString() == "SpiderSpawnRedo (UnityEngine.GameObject)" && undo == true)
                {
                    temp = Instantiate(tempPrefab);
                    Redo(ref tempPos.x, ref tempPos.y);
                    temp.transform.position = tempPos;
                    undo = false;
                }
            }

            // If the object is spawned get the position of the mouse and restrict it to the boundaries
            if (spawned == true)
            {
                Vector3 newPos = new Vector3(Mathf.Round(rayPoint.x), Mathf.Round(rayPoint.y), 0.0f);

                // Restricts it to the boundaries
                if (newPos.x > 5)
                    newPos.x = 5;
                if (newPos.x < -5)
                    newPos.x = -5;
                if (newPos.y > 5)
                    newPos.y = 5;
                if (newPos.y < -5)
                    newPos.y = -5;

                temp.transform.position = newPos;
            }
        }
        else
        {
            if (spawned == true)
            {
                tempPos = temp.transform.position;      // Save the postion
                spawned = false;
                Execute(ref tempPos.x, ref tempPos.y);  // Save the postion in the command pattern
            }
        }
    }
}
