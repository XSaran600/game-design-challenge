﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

// Author: Saran Krishnaraja 100621699

public class ECS_Centipede : MonoBehaviour
{
    const string DLL_NAME = "EntityComponentSystem";

    [DllImport(DLL_NAME)]
    private static extern void LoadDLL();

    [DllImport(DLL_NAME)]
    private static extern int Create();

    [DllImport(DLL_NAME)]
    private static extern void SetPos(int id, float x, float y, float z);

    [DllImport(DLL_NAME)]
    private static extern float GetX(int id);

    [DllImport(DLL_NAME)]
    private static extern float GetY(int id);

    [DllImport(DLL_NAME)]
    private static extern float GetZ(int id);

    int id;

    Vector3 tempPos;

    public float moveSpeed;

    private void Start()
    {
        LoadDLL();
        id = Create();

        SetPos(id, transform.position.x, transform.position.y, transform.position.z);
    }

    private void Update()
    {
        if (!Input.GetMouseButton(0))
        {
            tempPos = new Vector3(transform.position.x, transform.position.y - 0.1f * moveSpeed, 0);

            SetPos(id, tempPos.x, tempPos.y, tempPos.z);                    // Set the Position

            transform.position = new Vector3(GetX(id), GetY(id), GetZ(id)); // Get the X,Y,Z
        }
    }
}
